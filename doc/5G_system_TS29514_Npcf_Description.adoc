= 5G system TS29514 Npcf Protocol Modules for TTCN-3 Toolset with TITAN, Description
:author: Gábor Szalai
:toc: left

== About This Document

This is the description for the 5G system TS29514 Npcf protocol module. The 5G system TS29514 Npcf protocol modules are developed for the TTCN-3 Toolset with Titan. 

== Functionality

The 5G system TS29514 Npcf protocol module implements the object structures of the 3GPP TS 29514 v15.1.0<<_5, [5]>> in a formalized way, using the standard specification language TTCN-3. This allows defining of test data (templates) in the TTCN-3 language and correctly encoding/decoding messages when executing test suites using the Titan TTCN-3 test environment.
The 5G system TS29514 Npcf protocol module uses Titan’s JSON encoding <<_3, [3]>> and hence is usable with the Titan test toolset only.

=== System Requirements
Protocol modules are a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, protocol modules alone do not put specific requirements on the system used. However, in order to compile and execute a TTCN-3 test suite using the set of protocol modules the following system requirements must be satisfied:

* Titan TTCN-3 Test Executor version CRL 113 200/6 R5A (6.5.pl0) or higher installed. For Installation Guide see <<_2, [2]>>. Please note: This version of the test port is not compatible with Titan releases earlier than CRL 113 200/6 R5A.

===	Installation
The set of protocol modules can be used in developing TTCN-3 test suites using any text editor. Since the 5G system TS29514 Npcf protocol is used as a part of a TTCN-3 test suite, this requires TTCN-3 Test Executor be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

== Interface description

=== Encoding/decoding and other related functions
This product also contains encoding/decoding functions, which assure correct encoding of messages when sent from Titan and correct decoding of messages when received by Titan. 

==== Implemented encoding and decoding functions

[source]
----
Name	Type of formal parameters	Type of return value
f_enc_AppSessionContextReqData(in AppSessionContextReqData pdu) return octetstring 
f_dec_AppSessionContextReqData(in octetstring stream, out AppSessionContextReqData pdu) return integer 
f_enc_AppSessionContext(in AppSessionContext pdu) return octetstring 
f_dec_AppSessionContext(in octetstring stream, out AppSessionContext pdu) return integer 
f_enc_TerminationInfo(in TerminationInfo pdu) return octetstring 
f_dec_TerminationInfo(in octetstring stream, out TerminationInfo pdu) return integer 
f_enc_EventsNotification(in EventsNotification pdu) return octetstring 
f_dec_EventsNotification(in octetstring stream, out EventsNotification pdu) return integer 
f_enc_AppSessionContextUpdateData(in AppSessionContextUpdateData pdu) return octetstring 
f_dec_AppSessionContextUpdateData(in octetstring stream, out AppSessionContextUpdateData pdu) return integer 
f_enc_EventsSubscReqData(in EventsSubscReqData pdu) return octetstring 
f_dec_EventsSubscReqData(in octetstring stream, out EventsSubscReqData pdu) return integer 
----

== Usage

The protocol module provides abstract data types for JSON objects used by the services defined in the standard <<_5, [5]>>. The protocol module provides encoder and decoder functions for the JSON objects. The decoder function validates the received JSON object based on the JSON schema as well. Only the validated JSON value is decoded.

== Terminology	

=== Abbreviations

JSON:: JavaScript Object Notation

PDU:: Protocol Data Unit

TTCN-3:: Testing and Test Control Notation version 3

== References

[[_1]]
[1]	ETSI ES 201 873-1 v4.4.1 (2012-04) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2]	1/ 198 17-CRL 113 200/6 Uen +
Installation Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3]	2/198 17-CRL 113 200/6 Uen +
Programmer's Technical Reference for Titan TTCN-3 Test Executor

[[_4]]
[4]	https://www.json.org

[[_5]]
[5] 3GPP TS 29514 v15.1.0+
5G System; Policy Authorization Service; Stage 3

